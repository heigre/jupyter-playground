# Jupyter Playground

A small repo containing a dockerized jupyter notebook that is helpful 
for testing new libraries and features



# Getting Started


## Dependencies
Docker Compose


# Build and Test
From within the repository root directory:
```
$ docker compose up
```

## Access the Compute Module Jupyter Notebook
The compute module comes bundled with Jupyter notebook which makes building processing applications a breeze.  

To use Jupyter, you need to access the compute module container:

```
$ docker exec -it jupyter-playground-host bash
```

Then from within the container:

```
$ jupyter notebook --ip 0.0.0.0 --allow-root --notebook-dir=/vol/
```
This should start the jupyter notebook and provide an URL to the jupyter web server with an access token.

