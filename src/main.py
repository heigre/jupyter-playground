import asyncio


async def sleep_function():
    await asyncio.sleep(1)
    print("hearthbeat")


async def main():
    sleep_task = loop.create_task(sleep_function())
    await asyncio.wait([sleep_task])


if __name__ == "__main__":
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_forever()

